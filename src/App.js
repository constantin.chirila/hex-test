import React, { Component } from 'react';
import FlickrCard from './components/FlickrCard'
import config from './config';
import axios from 'axios'

class App extends Component {
  constructor(){
    super()
    this.state ={
      photostream:[],
      isFetching: true
    }
  }
  componentDidMount() {
    axios.get(config.api) 
    .then((response) => {
      this.setState({
        photostream: response.data.items,
        isFetching: false
      })
    })
    .catch((err) => {
    alert(err)
    })
  }
  render() {
    let listItems =[];
    if(this.state.photostream.length !== 0) {
      listItems = this.state.photostream.map((item, index) => 
        <FlickrCard 
        key={index}
        title={item.title}
        author={item.author}
        authorID={item.author_id}
        link={item.link}
        description={item.description}
        tags={item.tags}
        image={item.media.m}
        />
      )
    }
    
    return (
      <div className="main">
        <h1 className="mainTitle">Flickr Photo stream</h1>
        <section className="photostream">
          {this.state.isFetching ? 'Photostream is loading' : listItems}
        </section>
      </div>
    );
  }
}

export default App;
