import React, { Component } from 'react';

class FlickrCard extends Component {
    formatAuthor(){
        return this.props.author.replace('nobody@flickr.com','').trim().slice(2,-2)
    }
    formatTags(){
        return this.props.tags.replace(/ /g, ', ')
    }
    render() {
        return (
            <div className="flickrCard">
                <div className="imgContainer">
                    <a href={this.props.link} title={this.props.title}>
                        <img src={this.props.image} alt={this.props.title}/>
                    </a>
                </div>
                <a href={this.props.link} title={this.props.title}>{this.props.title}</a> by <a href={"https://www.flickr.com/photos/" + this.props.authorID}>{this.formatAuthor()}</a>
                <h3>Description</h3>
                <p>This is a placeholder photo description. The API presents it in html which is unsafe. Also it seems the Public API is unsuitable for this type of app. </p>
                {/* <div dangerouslySetInnerHTML={{ __html: this.props.description }} /> */}
                {this.props.tags.length > 0 ? (<p className="tags"><b>Tags: </b>{this.formatTags()}</p>): (<p className="tags">No tags to show</p>)}
            </div>
        );
    }
}

export default FlickrCard;